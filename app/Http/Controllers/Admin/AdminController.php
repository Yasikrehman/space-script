<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Hash;
use App\User;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
    	if(Session::get('admin')!=''){

        	return view('admin.dashboard');
    	} else {
    		return view('admin.admin_login');
    	}
    }

    public function login(Request $request){

    	$input = $request->all();

    	if(isset($input['username']) && isset($input['password'])){

    		$data = User::where('name',$input['username'])->first();

    		if(is_object($data)){
    			if(Hash::check($input['password'],$data->password)){
    				Session::put('admin',$data->id);
    				return redirect('admin');

    			} else {

    			}
    		}
    	} else {
    		echo 1;exit;
    	}
    }
}
